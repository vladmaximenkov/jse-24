package ru.vmaksimenkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.exception.empty.EmptyEmailException;
import ru.vmaksimenkov.tm.exception.user.EmailExistsException;
import ru.vmaksimenkov.tm.exception.user.LoginExistsException;
import ru.vmaksimenkov.tm.util.TerminalUtil;

import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

public final class UserRegistryCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Register new user";
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRY]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        if (serviceLocator.getUserService().existsByLogin(login)) throw new LoginExistsException();
        System.out.println("ENTER E-MAIL:");
        @NotNull final String email = TerminalUtil.nextLine();
        if (isEmpty(email)) throw new EmptyEmailException();
        if (serviceLocator.getUserService().existsByEmail(email)) throw new EmailExistsException();
        System.out.println("ENTER PASSWORD:");
        serviceLocator.getAuthService().registry(login, TerminalUtil.nextLine(), email);
    }

    @NotNull
    @Override
    public String name() {
        return "user-registry";
    }

}
