package ru.vmaksimenkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.exception.entity.TaskNotFoundException;
import ru.vmaksimenkov.tm.util.TerminalUtil;

public final class TaskByProjectIdBindCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Bind task by project id";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[BIND TASK TO PROJECT BY ID]");
        if (serviceLocator.getTaskService().size() < 1) throw new TaskNotFoundException();
        System.out.println("ENTER TASK INDEX:");
        @Nullable final String taskId = serviceLocator.getTaskService().getIdByIndex(userId, TerminalUtil.nextNumber());
        if (taskId == null) return;
        System.out.println("ENTER PROJECT ID:");
        serviceLocator.getProjectTaskService().bindTaskByProjectId(userId, TerminalUtil.nextLine(), taskId);
    }

    @NotNull
    @Override
    public String name() {
        return "task-bind";
    }

}
