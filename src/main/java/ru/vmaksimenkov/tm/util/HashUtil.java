package ru.vmaksimenkov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface HashUtil {

    @NotNull Integer ITERATION = 20000;
    @NotNull String SECRET = "XwsSAdc6wZj2pSmi7";

    @Nullable
    static String md5(@Nullable final String s) {
        if (s == null) return null;
        try {
            @NotNull final java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            @NotNull final byte[] array = md.digest(s.getBytes());
            @NotNull final StringBuilder sb = new StringBuilder();
            for (final byte b : array) {
                sb.append(Integer.toHexString((b & 0xFF) | 0x100), 1, 3);
            }
            return sb.toString();
        } catch (@NotNull final java.security.NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    @Nullable
    static String salt(@Nullable final String s) {
        if (s == null) return null;
        @Nullable String result = s;
        for (int i = 0; i < ITERATION; i++) {
            result = md5(SECRET + result + SECRET);
        }
        return result;
    }

}